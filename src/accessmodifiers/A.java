package accessmodifiers;

public class A {

	public int y = 1;
	protected int z = 1;
	int w = 1;
	private int x = 1;

	public int getX() {

		return x;
	}

	public void setX(int x) {
		this.x = x;
	}
}
