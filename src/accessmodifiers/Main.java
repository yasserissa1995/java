package accessmodifiers;

public class Main {

	public static void main(String[] a) {

		System.out.println(new A().getX()); // 1 // private  : same class
		System.out.println(new A().y); // 1 //  public : anywhere
		System.out.println(new B().z); // 1 //  protected same package
		System.out.println(new B().w); // 1 //  default or without : same package
		System.out.println(new B().e); // 1 //  default or without : same package

	}
}
