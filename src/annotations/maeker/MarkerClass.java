package annotations.maeker;

public interface MarkerClass {

	@Deprecated
	static void deprecatedMethod() {
	}

	@Override  // ex marker annotation
	String toString();

	@SuppressWarnings("deprecation")
	default void useDeprecatedMethod() {
		// deprecation warning
		// - suppressed
		deprecatedMethod();
	}

}
