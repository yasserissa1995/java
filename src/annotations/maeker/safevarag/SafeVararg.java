package annotations.maeker.safevarag;

import java.util.Arrays;

public class SafeVararg {

	@SafeVarargs
	private static void printList(int... x) { // Variable Arguments (Varargs) in Java

		Arrays.stream(x).forEach(System.out::println);
	}

	@SafeVarargs
	private static void printList(String s, int... x) { // Variable Arguments (Varargs) in Java
		System.out.println(s);
		Arrays.stream(x).forEach(System.out::println);
	}

	//	private static void printList(int... x, String s) { // compilation errors : Vararg parameter must be the last in the list
	//	}

	//	private static void printList(String... s, int... x) { // -	There can be only one variable argument in a method
	//	}

	public static void main(String[] v) {

		printList(100, 200);

		printList("test", 100, 200, 300);

	}

}
