package datatyps;

import datatyps.enums.Day;
import datatyps.enums.EmployeeType;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

	public static void main(String[] a) {

		//        integerEx();
		//        doubleEx();
		//        floatEx();
		//        longEx();
		//        shortEx();
		//        byteEx();
		//        booleanEx();
		//        charEx();
		//        bigDecimalEx();
		//        bigIntegerEx();
		//        stringAndStringBuilderAndBufferEx();
		//        implicitCastingEx();
		//        explicitCastingEx();
		//        randomEx();
		enumEx();

	}

	private static void enumEx() {

		Day day = Day.FRIDAY;

		System.out.println(day);  // FRIDAY
		System.out.println(day.getDayNumber()); // 6

		EmployeeType employeeType = EmployeeType.FULL_TIME;

		System.out.println(employeeType); // FULL_TIME
		System.out.println(employeeType.getaClass().getName()); // FullTimeEmployee
		System.out.println(employeeType.getDesc()); // full time employee
	}

	private static void randomEx() {

        /* random class
          - need to create an instance
          - support integers, longs, float, double and boolean
          - Random class has only 48 bits
        */
		System.out.println(new Random().nextDouble());       // random double number ex  0.3173153624750281
		System.out.println(new Random().nextInt());          // random int pos-neg number ex  -301749577 or 301749577
		System.out.println(new Random().nextLong());         // random long pos-neg number ex   1345123919128620827 or -1345123919128620827
		System.out.println(new Random().nextBoolean());      // random boolean value ex   true or false
		System.out.println(new Random().nextInt(25)); // Random integer value from 0 to 25

      /* Math.random()
          - do not need to create an instance
          - suport double and need some code to get integer value or long
          - thread safe
          - not secure
      */
		System.out.println(Math.random()); // random double number ex  0.3173153624750281
		System.out.println((int) Math.floor(Math.random() * (25 + 1) + 0)); // Random integer value from 0 to 25

        /* ThreadLocalRandom :
            - if you are sharing your Random number generator among multiple threads.
            - not secure
        */
		System.out.println(ThreadLocalRandom.current().nextInt(25)); // Random integer value from 0 to 25 and thread safe

        /* SecureRandom
           - SecureRandom can have upto 128 bits
           - probability of repeating in SecureRandom are smaller
           - in security-sensitive applications.
        */
		System.out.println(new SecureRandom().nextInt(25)); // Random integer value from 0 to 25
	}

	private static void implicitCastingEx() {

        /*
          int :  short , byte , char
         long :  int
         float :  long
         double = float
        */
		System.out.println("[int =>  short , byte , char] , [long =>  int] , [float =>  long]  , [double => float] ");
	}

	private static void explicitCastingEx() {

		System.out.println((int) 10.5);  // 10
		System.out.println((int) 10L);    // 10
		System.out.println((char) 67); // C
	}

	private static void stringAndStringBuilderAndBufferEx() {

		/*
		                    String                   StringBuilder            StringBuffer
		     Storage        - Heap memory and pool    - Heap memory             - Heap memory
		     Object         - immutable               - mutable                 - mutable
             Memory         - if we change the value  - if we change the value  - if we change the value
                              of string that will be  - of string that will not   of string that will not
                              created a new strings      creat a new strings      creat a new strings
            Tread safe       - no                      - yes                       - no
		 */

		/*
		 JVM checks literal string in pool
		      - if present : return reference
			  - otherwise: create a new String in pool and return reference
		 declaring String using new keyword
		    - store in heap memory not in string pool
		 declaring String using new keyword and intern()
		     - make new string pointer on pool
        */
		String x1 = "a";
		String x2 = "a";
		String x3 = "a";
		String x4 = "a";

		System.out.println(x1 == x2);           // true (same address ?)
		System.out.println(x1 == x3);           // false (same address ? )
		System.out.println(x1.equals(x3));      // true (same value ? )
		System.out.println(x3.equals(x4));      // true (same value ? )
		System.out.println(x3 == x4);           // false (same address ? )

		StringBuilder s1 = new StringBuilder("123");
		StringBuffer s2 = new StringBuffer("123");

		// System.out.println("s1 == s2 :: " + (s1 == s2));  // not valid

		System.out.println(s1.equals(s2));  //  false
		System.out.println(s1.toString().equals(s2.toString()));  // true
	}

	private static void bigIntegerEx() { //  BigInteger : all available primitive data types

		int int1 = 20, int2 = 10;

		BigInteger BigInteger1 = new BigInteger("20");
		BigInteger BigInteger2 = BigInteger.valueOf(10);

		System.out.println(int1 - int2);   // 10
		System.out.println(BigInteger1.add(BigInteger2)); // 30
	}

	private static void bigDecimalEx() {

		double double1 = 0.03;
		double double2 = 0.04;
		System.out.println(double1 - double2);

		Double Double1 = 0.03;
		Double Double2 = 0.04;
		System.out.println(Double1 - Double2);

		BigDecimal BigDecimal1 = new BigDecimal("0.0151"); // string init for scaling
		BigDecimal BigDecimal12 = new BigDecimal("0.04");
		System.out.println(BigDecimal1.subtract(BigDecimal12)); //
	}

	private static void charEx() {  // char 2 bytes Stores a single character

		char x1 = 1;
		char x2 = 11111;
		// char x3 = 111111; // not valid size
		// char x4 = "5";   // not valid
		char x5 = '1';
		// char x6 = '11';  // not calid size
		char x7 = '*';
		// char x8 = '**';  // not valid size

		// Char x7 = '*';  // not valid identifier
		Character x9 = 1;  // autoboxing
		// Character x10 = "11"; // not valid
	}

	private static void booleanEx() {  // boolean 1 bit Stores true or false values

		// boolean x1 = 1;    // not valid
		//  boolean x2 = 1b; // not valid
		boolean x3 = false;
		boolean x4 = Boolean.TRUE;
		Boolean x5 = true; // autoboxing
		Boolean x6 = Boolean.TRUE;
	}

	private static void byteEx() { // byte 1 byte : 8-bit signed two's

		byte x1 = 1;
		byte x2 = 111;
		Byte x3 = 111;
		// byte x4 = 1111; // not valid

	}

	private static void shortEx() { // short 2 bytes : 16-bit signed two's

		short x1 = 1;
		short x2 = 10000;
		// short x3 = 100000; // not valid
		// short x4 = 5L;     // not valid
		// short x5 = 1.0;   // not valid
	}

	private static void longEx() { // long 8 bytes

		long x1 = 5;        // valid : implicit casting
		// long x2 = 5.0;  // not valid
		long x3 = 5L;      // valid
		long x4 = 5L;     // valid
		// long x5 = 55F; // not valid
		// Long x6 = 5;   // not valid
		Long x7 = 5L;     //  valid
	}

	private static void doubleEx() { // double 8 bytes Stores fractional numbers.Sufficient for storing 15 decimal digits

		double x1 = 5;    // valid : implicit casting
		// Double x2 = 5; // not valid
		Double x3 = 5.0;  // autoboxing
		// Double x4 = 5.0F; // not valid
		double x5 = 5.0F;  // valid
		double x6 = 5.0f;  // valid
		double x7 = 55L;  // valid
		double x8 = 55L;  // valid
	}

	private static void floatEx() { // float 4 bytes Stores fractional numbers.Sufficient for storing 6 to 7 decimal digits

		float x1 = 5;   // valid : implicit casting
		//  float x2 = 5.0;  // not valid
		float x3 = 5.0F;  // valid
		float x4 = 5.0f;  // valid
		float x5 = 55L;   // valid
		float x6 = 55L;   // valid
		// float x7 = true;  // not valid
		//  Float x8 = 5;    // not valid
		// Float x9 = 55L;  // not valid
		// Float x10 = 5.0; // not valid
		Float x11 = 55.0f;  // valid
		Float x12 = 55.0F;  // valid
	}

	private static void integerEx() { // integer 4 bytes

		int x1 = 5;      // valid
		Integer x2 = 5; // valid autoboxing
	}
}
