package datatyps.enums;

public enum EmployeeType {
	FULL_TIME(FullTimeEmployee.class, "full time employee"), PART_TIME(PartTimeEmployee.class, "part time employee");
	private final Class aClass;
	private final String desc;

	EmployeeType(Class<?> aClass, String desc) {
		this.aClass = aClass;
		this.desc = desc;
	}

	public Class getaClass() {
		return aClass;
	}

	public String getDesc() {
		return desc;
	}
}
