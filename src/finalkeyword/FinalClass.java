package finalkeyword;

public final class FinalClass {

	private final String str;

	public FinalClass(String str) {
		this.str = str;
	}

	public String trim() {

		if (this.str != null) {
			return this.str.trim();
		}

		throw new NullPointerException("str is null");
	}
}