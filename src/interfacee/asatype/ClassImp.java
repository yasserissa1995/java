package interfacee.asatype;

public class ClassImp implements Interface1, Interface2 {

	public static void main(String[] d) {

		ClassImp c = new ClassImp();

		boolean isClass = (c instanceof ClassImp);   //true
		boolean isInterface1 = (c instanceof Interface1); //true
		boolean isInterface2 = (c instanceof Interface2); //true

	}
}
