package interfacee.defaultMethod;

public class InterfaceImp implements Print2 {

	@Override public void exportPDF(String text) {

	}

	@Override public void reportWord(String text) {

	}

	// Redefine the default method, which overrides it.
	@Override public void reportExcel(String text) {

		System.out.println("from implementer " + text);
		Print2.super.reportExcel(text);
	}
}
