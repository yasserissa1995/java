package interfacee.defaultMethod;

public interface Print {

	void exportPDF(String text);

	void reportWord(String text);

	// at a later time, you want to add a third method to export excel
	default void reportExcel(String text) {

		System.out.println(text);
	}
}
