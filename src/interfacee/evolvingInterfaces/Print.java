package interfacee.evolvingInterfaces;

public interface Print {

	void exportPDF(String text);

	void reportWord(String text);
}
