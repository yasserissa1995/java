package interfacee.evolvingInterfaces;

public interface Print2 extends Print {

	// at a later time, you want to add a third method to export excel
	void exportExcel(String text);

}
