package interfacee.ex1;

public interface Interface1 {

	double E = 2.718282; // public static final by default

	void doSomething(int i, double x); // public by default

	int doSomethingElse(String s);
}
