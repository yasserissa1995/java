package interfacee.inerfaceMethod;

public class Client extends Horse implements Flyer, Mythical {

    // Instance methods are preferred over interface default methods.
    public static void main(String... args) {
        Client myApp = new Client();
        System.out.println(myApp.identifyMyself()); // I am a horse.
    }
}
