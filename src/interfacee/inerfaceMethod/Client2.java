package interfacee.inerfaceMethod;

public class Client2 implements EggLayer, FireBreather {
    // Methods that are already overridden by other candidates are ignored. This circumstance can arise when supertypes share a common ancestor.
    public static void main(String... args) {

        Client2 myApp = new Client2();
        System.out.println(myApp.identifyMyself()); // I am able to lay eggs.

    }
}
