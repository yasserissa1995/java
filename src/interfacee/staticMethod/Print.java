package interfacee.staticMethod;

public interface Print {

	// at a later time, you want to add a third method to export excel
	static void reportExcel(String text) {

	}

	void exportPDF(String text);

	void reportWord(String text);
}
