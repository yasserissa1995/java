package nestedclasses.anonymous;

public class Anonymous {

	public static final int finalField = 10;
	public static int x = 10;
	public static int staticField = 10;

	public void applyAnonymous() {

		AnonymousClass anonymousClass = new AnonymousClass() {

			final String name = "yasser";

			@Override public void write() {
				x = 20;
				System.out.println("name = " + name);
				System.out.println("x = " + x);
				System.out.println("finalField = " + finalField);
			}
		};
	}

	public interface AnonymousClass {

		void write();
	}
}