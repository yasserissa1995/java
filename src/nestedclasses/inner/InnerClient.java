package nestedclasses.inner;

public class InnerClient {

	public static void main(String[] args) {

		new OuterClass().new PublicInnerClass().printClassName();

	}
}