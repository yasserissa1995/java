package nestedclasses.inner;

public class OuterClass {

	public static int staticField = 20;
	public int publicField = 20;
	private final int privateField = 10;

	public void printClassName() {

		System.out.println(getClass().getName());
	}

	public class PublicInnerClass {

		public PublicInnerClass() {
			super();
		}

		public void print() {

			System.out.println("privateField = " + privateField);
			System.out.println("publicField = " + publicField);
			System.out.println("staticField = " + staticField);
		}

		public void printClassName() {

			System.out.println(getClass().getName()); // print inner class name
			//	OuterClass.this.printClassName(); // print outer class name
		}

	}

	private class PrivateInnerClass { // If you don't want outside objects to access the inner class, declare the class as private

		public PrivateInnerClass() {
			super();
		}

		public void print() {

			System.out.println("privateField = " + privateField);
			System.out.println("publicField = " + publicField);
			System.out.println("staticField = " + staticField);
		}

		public void printClassName() {

			System.out.println(getClass().getName()); // print inner class name
			//	OuterClass.this.printClassName(); // print outer class name
		}

	}

}
