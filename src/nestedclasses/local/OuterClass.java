package nestedclasses.local;

public class OuterClass {

	public static int staticField = 20;
	public int publicField = 20;
	private final int privateField = 10;

	public void printClassName() {

		System.out.println(getClass().getName());
	}

	public void apply() {

		class LocalClass {

			public LocalClass() {
				super();
			}

			public void print() {

				System.out.println("privateField = " + privateField);
				System.out.println("publicField = " + publicField);
				System.out.println("staticField = " + staticField);
			}

			public void printClassName() {

				System.out.println(getClass().getName());   // local class
				System.out.println(this.getClass().getName()); // local class
				//	thiskeyword.printClassName(); // call itself (recursion)
				OuterClass.this.printClassName(); // outer class
			}
		}

		LocalClass localClass = new LocalClass();
		localClass.printClassName();
	}
}