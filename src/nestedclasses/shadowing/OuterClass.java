package nestedclasses.shadowing;

public class OuterClass {

	private final int x = 10;

	public void apply() {

		int x = 20;
		class LocalClass {

			final int x = 30;

			public LocalClass() {
				super();
			}

			public void printX() {

				System.out.println("x = " + x);
				System.out.println("this.x = " + this.x);
				System.out.println("OuterClass.this.x = " + OuterClass.this.x);
			}
		}

		LocalClass localClass = new LocalClass();
		localClass.printX();
	}
}