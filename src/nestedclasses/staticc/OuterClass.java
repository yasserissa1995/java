package nestedclasses.staticc;

public class OuterClass {

	public static int staticField = 20;
	public int publicField = 20;
	private final int privateField = 10;

	public static void printOuterClass() {

		System.out.println(OuterClass.class.getName());
	}

	public static class PublicStaticClass {

		public PublicStaticClass() {
			super();
		}

		public static void printOuterClass() {

			System.out.println(OuterClass.PublicStaticClass.class.getName()); // print static class name using outer class
			System.out.println(PublicStaticClass.class.getName()); // print static class name
			OuterClass.printOuterClass(); // print outer class name
		}

		public void print() {

			//	System.out.println("privateField = " + privateField); // compilation error
			//	System.out.println("publicField = " + publicField);   // compilation error
			System.out.println("staticField = " + staticField);
		}
	}
}
