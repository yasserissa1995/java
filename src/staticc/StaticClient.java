package staticc;

import static staticc.StaticExample.staticImportField;

public class StaticClient {

	public static void main(String[] c) {

		StaticExample.printX(); // 1

		System.out.println(staticImportField); // static import

		StaticExample2.printX();

		StaticExample.printX();

	}

}
