package staticc;

public class StaticExample {

	public static int x = 0;
	public static int staticImportField = 0;

	// public int static y =10; // compilation error

	// static block : run when class is loading and used for initialize static members

	static {
		System.out.println("static block 1");
	}

	static {
		System.out.println("static block 2");
	}

	// static method
	public static void printX() {
		x++;
		System.out.println(x);
		// System.out.println(StaticExample.this.x);// compliation error => this belongs to instance and static belongs to class
		// System.out.println(this.x); // compliation error => this belongs to instance and static belongs to class
	}
}
