package staticc;

public class StaticExample2 extends StaticExample {

	//	@Override compilation error => static method cannot be Override
	public static void printX() {
		x++;
		System.out.println(x);
	}
}
