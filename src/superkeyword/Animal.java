package superkeyword;

public class Animal {

	private String name;

	public Animal(String name) {

		System.out.println("args constructor animal is started");
		this.name = name;
	}

	public Animal() { // for subclasses

		System.out.println("no args constructor animal is started");
	}

	public String getName() {
		return name;
	}
}
