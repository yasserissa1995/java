package superkeyword;

public class Dog extends Animal {

	public Dog(String name) {
		super(name);
		System.out.println("args constructor dog is started");
	}

	public Dog( ) {

		System.out.println("no args constructor  dog is started");
	}
}
