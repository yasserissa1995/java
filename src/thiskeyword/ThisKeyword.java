package thiskeyword;

public class ThisKeyword {

	public int x, y;

	public ThisKeyword(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public ThisKeyword() {

		this(10, 20);
	}

	public ThisKeyword(int x) {

		this(x, 20);
	}
}
